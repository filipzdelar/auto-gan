import os

import torch
import torch.nn as nn

import torch.optim as optim
import torch.backends.cudnn as cudnn

from torchvision.utils import save_image
import torchvision.datasets as dateset
import torchvision.transforms as transforms


from Models.Model128_D.discriminator import Discriminator
from Models.Model128_D.generator import Generator

import random
manualSeed = 9999
random.seed(manualSeed)
torch.manual_seed(manualSeed)


dataset_path = "DateSet/Cars/"
batch_size = 128 
image_size = 128
epochs = 30
learning_rate_D = 0.00018
learning_rate_G = 0.0002
noise = 100


images = dateset.ImageFolder(root = dataset_path,
                            transform = transforms.Compose([
                            transforms.Resize([image_size, image_size]),
                            transforms.ToTensor(),
                            transforms.Normalize((0.485, 0.456, 0.406), (0.229, 0.224, 0.225))]))


dataloader = torch.utils.data.DataLoader(images, batch_size = batch_size, shuffle=True,num_workers = 2)
device = torch.device("cuda:0")


generator = Generator(1, noise).to(device)
discriminator = Discriminator(1).to(device)

criterion = nn.BCELoss()

latent_vector = torch.rand(image_size, noise, 1, 1, device = device)


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0)

discriminator.apply(weights_init)
generator.apply(weights_init)

optimizer_discriminator = optim.Adam(discriminator.parameters(), lr = learning_rate_D, betas = (0.5, 0.999), amsgrad=True)
optimizer_generator = optim.Adam(generator.parameters(), lr = learning_rate_G, betas = (0.5, 0.999), amsgrad=True)

discriminator_error = []
generator_error = []

label_of_zeros = torch.zeros((batch_size,), device = device) 
label_of_ones = 0.1 * torch.rand((batch_size,), device = device) + 0.9 

fixed_noise = torch.randn(64, noise, 1, 1, device = device)

def discriminator_train_real():
    global error_real_epoche_discriminator, score_real_discriminator

    real_output = discriminator(pass_images).view(-1)
    error_real_epoche_discriminator = criterion(real_output, label_of_ones)
    error_real_epoche_discriminator.backward()
    score_real_discriminator = real_output.mean().item()

def discriminator_train_fake():
    global fake_images, error_fake_epoche_discriminator, score_fake_discriminator, error_epoche_discriminator

    noise_generate = torch.randn(batch_size, noise, 1, 1, device = device) 
    fake_images = generator(noise_generate)
    
    fake_output = discriminator(fake_images.detach()).view(-1)
    
    error_fake_epoche_discriminator = criterion(fake_output, label_of_zeros)
    error_fake_epoche_discriminator.backward()
    score_fake_discriminator = fake_output.mean().item()
    
    error_epoche_discriminator = error_real_epoche_discriminator + error_fake_epoche_discriminator
    optimizer_discriminator.step()


def generator_train():
    global error_epoche_generator
    
    output = discriminator(fake_images).view(-1)

    error_epoche_generator = criterion(output, label_of_ones)
    error_epoche_generator.backward()
    score_generator = output.mean().item() 
    optimizer_generator.step()

def summerize_iteration():
    global discriminator_error, generator_error
    
    discriminator_error.append(error_fake_epoche_discriminator + error_real_epoche_discriminator)
    generator_error.append(error_epoche_generator)
    if iteration % 25 == 0:
        print("Epoha [{}/{}] Iteracija [{}/{}]D: {:.4f} G: {:.4f}".format(epoch, epochs, iteration, len(dataloader), error_fake_epoche_discriminator + error_real_epoche_discriminator, error_epoche_generator))
        
def summerize_epoch():
    with torch.no_grad():
        fake_images = generator(fixed_noise).detach().cpu()

    save_image(fake_images[0], '0gan{}.png'.format(epoch), padding=2, normalize=True)
    save_image(fake_images[1], '1gan{}.png'.format(epoch), padding=2, normalize=True)
    save_image(fake_images[2], '2gan{}.png'.format(epoch), padding=2, normalize=True)
    save_image(fake_images[3], '3gan{}.png'.format(epoch), padding=2, normalize=True)

    if epoch % 5 == 0:
        torch.save(discriminator.state_dict(), "./discriminator"+str(epoch)+".pth")
        torch.save(generator.state_dict(), "./generator"+str(epoch)+".pth")

    


for epoch in range(epochs):
    iteration = 0

    for batch in dataloader:
        iteration += 1
        
        pass_images = batch[0].to(device)
        if pass_images.size(0) != batch_size:
            continue

        # Train Discriminator
        discriminator.zero_grad()
        discriminator_train_real()
        discriminator_train_fake()

        # Train Generator
        generator.zero_grad()
        generator_train()

        summerize_iteration()

    summerize_epoch()
    if epoch > 9 and error_epoche_generator > 30:
        break
    
    
    

torch.save(discriminator.state_dict(), "./discriminator.pth")
torch.save(generator.state_dict(), "./generator.pth")

import matplotlib.pyplot as plt

plt.figure(figsize=(10,5))
plt.title("Greške generatora i diskriminatora")
plt.plot(generator_error, label="G")
plt.plot(discriminator_error, label="D")
plt.xlabel("iteracija")
plt.ylabel("greška")
plt.legend()
plt.show()