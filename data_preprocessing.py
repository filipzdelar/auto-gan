import os 
  
def generate_name(count):
    return "0" * (9 - len(str(count))) + str(count) + ".jpg"

def rename_all_files(): 
  
    for count, filename in enumerate(os.listdir("DateSet/Cars/Cars/")): 
        os.rename("DateSet/Cars/Cars/"+filename, "DateSet/Cars/Cars/"+generate_name(count)) 

if __name__ == '__main__': 
    
    rename_all_files() 
